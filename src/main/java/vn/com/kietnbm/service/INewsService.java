package vn.com.kietnbm.service;

import java.util.List;

import vn.com.kietnbm.model.NewsModel;
import vn.com.kietnbm.paging.Pageble;

public interface INewsService {
    List<NewsModel> findByCategoryId(Long categoryId);

    NewsModel save(NewsModel newsModel);

    List<NewsModel> findAll(Pageble pageble);

    int getTotalItem();
}
