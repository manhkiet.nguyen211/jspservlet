package vn.com.kietnbm.service;

import vn.com.kietnbm.dao.IGenericDAO;
import vn.com.kietnbm.model.UserModel;

public interface IUserService extends IGenericDAO<UserModel> {
    public UserModel findByUserNameAndPasswordAndStatus(String userName,
            String password, Integer status);
}
