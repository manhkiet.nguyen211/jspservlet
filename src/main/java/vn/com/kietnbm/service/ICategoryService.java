package vn.com.kietnbm.service;

import java.util.List;

import vn.com.kietnbm.model.CategoryModel;

public interface ICategoryService {
    List<CategoryModel> findAll();
}
