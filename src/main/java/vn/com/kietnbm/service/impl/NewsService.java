package vn.com.kietnbm.service.impl;

import java.util.List;

import javax.inject.Inject;

import vn.com.kietnbm.dao.INewsDAO;
import vn.com.kietnbm.model.NewsModel;
import vn.com.kietnbm.paging.Pageble;
import vn.com.kietnbm.service.INewsService;

public class NewsService implements INewsService {

    @Inject
    private INewsDAO newsDAO;

    @Override
    public List<NewsModel> findByCategoryId(Long categoryId) {
        return newsDAO.findByCategoryId(categoryId);
    }

    @Override
    public NewsModel save(NewsModel newsModel) {
        Long newId = newsDAO.save(newsModel);
        System.out.println(newId);
        return null;
    }

    @Override
    public List<NewsModel> findAll(Pageble pageble) {
        return newsDAO.findAll(pageble);
    }

    @Override
    public int getTotalItem() {
        return newsDAO.getTotalItem();
    }

}
