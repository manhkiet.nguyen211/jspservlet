package vn.com.kietnbm.service.impl;

import javax.inject.Inject;

import vn.com.kietnbm.dao.IUserDAO;
import vn.com.kietnbm.dao.impl.AbstractDAO;
import vn.com.kietnbm.model.UserModel;
import vn.com.kietnbm.service.IUserService;

public class UserService extends AbstractDAO<UserModel>
        implements IUserService {

    @Inject
    private IUserDAO userDAO;

    @Override
    public UserModel findByUserNameAndPasswordAndStatus(String userName,
            String password, Integer status) {
        return userDAO.findByUserNameAndPasswordAndStatus(userName, password,
                status);
    }

}
