package vn.com.kietnbm.service.impl;

import java.util.List;

import javax.inject.Inject;

import vn.com.kietnbm.dao.ICategoryDAO;
import vn.com.kietnbm.model.CategoryModel;
import vn.com.kietnbm.service.ICategoryService;

public class CategoryService implements ICategoryService {

    @Inject
    private ICategoryDAO categoryDAO;

    @Override
    public List<CategoryModel> findAll() {
        List<CategoryModel> results = categoryDAO.findAll();
        return results;
    }

}
