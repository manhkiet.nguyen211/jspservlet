package vn.com.kietnbm.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import vn.com.kietnbm.model.RoleModel;
import vn.com.kietnbm.model.UserModel;

public class UserMapper implements RowMapper<UserModel> {

    @Override
    public UserModel mapRow(ResultSet rs) {
        try {
            UserModel userModel = new UserModel();
            userModel.setId(rs.getLong("id"));
            userModel.setUserName(rs.getString("username"));
            userModel.setPassword(rs.getString("password"));
            userModel.setFullName(rs.getString("fullname"));
            userModel.setStatus(rs.getInt("status"));
            userModel.setRoleId(rs.getLong("roleid"));
            try {
                RoleModel role = new RoleModel();
                role.setCode(rs.getString("code"));
                role.setName(rs.getString("name"));
                userModel.setRoleModel(role);
            } catch (Exception e) {
                e.printStackTrace();
            }
            userModel.setCreateDate(rs.getTimestamp("createddate"));
            userModel.setCreateBy(rs.getString("createdby"));
            if (rs.getTimestamp("modifieddate") != null) {
                userModel.setModifyDate(rs.getTimestamp("modifieddate"));
            }
            if (rs.getString("modifiedby") != null) {
                userModel.setModifyBy(rs.getString("modifiedby"));
            }
            return userModel;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
