package vn.com.kietnbm.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import vn.com.kietnbm.model.NewsModel;

public class NewsMapper implements RowMapper<NewsModel> {

    @Override
    public NewsModel mapRow(ResultSet rs) {
        try {
            NewsModel newsModel = new NewsModel();
            newsModel.setId(rs.getLong("id"));
            newsModel.setTitle(rs.getString("title"));
            newsModel.setContent(rs.getString("content"));
            newsModel.setCategoryId(rs.getLong("categoryid"));
            newsModel.setThumbnail(rs.getString("thumbnail"));
            newsModel.setShortDescription(rs.getString("shortdescription"));
            newsModel.setCreateDate(rs.getTimestamp("createddate"));
            newsModel.setCreateBy(rs.getString("createdby"));
            if (rs.getTimestamp("modifieddate") != null) {
                newsModel.setModifyDate(rs.getTimestamp("modifieddate"));
            }
            if (rs.getString("modifiedby") != null) {
                newsModel.setModifyBy(rs.getString("modifiedby"));
            }
            return newsModel;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
