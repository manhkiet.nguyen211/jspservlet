package vn.com.kietnbm.paging;

import vn.com.kietnbm.sort.Sorter;

public interface Pageble {
    Integer getPage();

    Integer getLimit();

    Integer getOffset();

    Sorter getSorter();
}
