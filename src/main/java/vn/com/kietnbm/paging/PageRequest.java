package vn.com.kietnbm.paging;

import vn.com.kietnbm.sort.Sorter;

public class PageRequest implements Pageble {
    private Integer page;
    private Integer maxPageItem;
    private Sorter sort;

    public PageRequest(Integer page, Integer maxPageItem, Sorter sort) {
        super();
        this.page = page;
        this.maxPageItem = maxPageItem;
        this.sort = sort;
    }

    @Override
    public Integer getPage() {
        return this.page;
    }

    @Override
    public Integer getLimit() {
        return this.maxPageItem;
    }

    @Override
    public Integer getOffset() {
        if (this.page != null && this.maxPageItem != null) {
            return (this.page - 1) * this.maxPageItem;
        } else {
            return null;
        }
    }

    @Override
    public Sorter getSorter() {
        return this.sort;
    }

}
