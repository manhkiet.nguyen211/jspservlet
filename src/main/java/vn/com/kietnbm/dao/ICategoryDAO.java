package vn.com.kietnbm.dao;

import java.util.List;

import vn.com.kietnbm.model.CategoryModel;

public interface ICategoryDAO extends IGenericDAO<CategoryModel> {
    List<CategoryModel> findAll();
}
