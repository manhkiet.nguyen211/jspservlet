package vn.com.kietnbm.dao;

import vn.com.kietnbm.model.UserModel;

public interface IUserDAO extends IGenericDAO<UserModel> {
    UserModel findByUserNameAndPasswordAndStatus(String userName,
            String password, Integer status);
}
