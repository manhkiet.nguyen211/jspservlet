package vn.com.kietnbm.dao.impl;

import java.util.List;

import vn.com.kietnbm.dao.INewsDAO;
import vn.com.kietnbm.mapper.NewsMapper;
import vn.com.kietnbm.model.NewsModel;
import vn.com.kietnbm.paging.Pageble;

public class NewsDAO extends AbstractDAO<NewsModel> implements INewsDAO {

    @Override
    public List<NewsModel> findByCategoryId(Long categoryId) {
        String sql = "select * from news where categoryid = ?";
        return query(sql, new NewsMapper(), categoryId);
    }

    @Override
    public Long save(NewsModel newsModel) {
        String sql = "insert into news(title, content, categoryid) values(?, ?, ?)";
        return insert(sql, newsModel.getTitle(), newsModel.getContent(),
                newsModel.getCategoryId());
    }

    @Override
    public List<NewsModel> findAll(Pageble pageble) {
        // String sql = "select * from news limit ?, ?";
        StringBuilder sql = new StringBuilder("select * from news");
        if (pageble.getSorter().getSortName() != null
                && pageble.getSorter().getSortBy() != null) {
            sql.append(" order by " + pageble.getSorter().getSortName() + " "
                    + pageble.getSorter().getSortBy());
        }
        if (pageble.getOffset() != null && pageble.getLimit() != null) {
            sql.append(" limit " + pageble.getOffset() + ", "
                    + pageble.getLimit());
        }
        return query(sql.toString(), new NewsMapper());
    }

    @Override
    public int getTotalItem() {
        String sql = "select count(*) from news";
        return count(sql);
    }

}
