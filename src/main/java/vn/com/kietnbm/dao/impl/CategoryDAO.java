package vn.com.kietnbm.dao.impl;

import java.util.List;

import vn.com.kietnbm.dao.ICategoryDAO;
import vn.com.kietnbm.mapper.CategoryMapper;
import vn.com.kietnbm.model.CategoryModel;

public class CategoryDAO extends AbstractDAO<CategoryModel> implements ICategoryDAO {

    @Override
    public List<CategoryModel> findAll() {
        String sql = "select * from category";
        return query(sql, new CategoryMapper());
    }

}
