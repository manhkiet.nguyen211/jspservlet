package vn.com.kietnbm.dao.impl;

import java.util.List;

import vn.com.kietnbm.dao.IUserDAO;
import vn.com.kietnbm.mapper.UserMapper;
import vn.com.kietnbm.model.UserModel;

public class UserDAO extends AbstractDAO<UserModel> implements IUserDAO {

    @Override
    public UserModel findByUserNameAndPasswordAndStatus(String userName,
            String password, Integer status) {
        StringBuilder sql = new StringBuilder("select * from user as u");
        sql.append(" inner join role as r on u.roleid = r.id");
        sql.append(" where u.username = ? and u.password = ? and u.status = ?");

        List<UserModel> listUser = query(sql.toString(), new UserMapper(),
                userName, password, status);
        return listUser.isEmpty() ? null : listUser.get(0);
    }

}
