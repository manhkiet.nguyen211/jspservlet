package vn.com.kietnbm.dao;

import java.util.List;

import vn.com.kietnbm.model.NewsModel;
import vn.com.kietnbm.paging.Pageble;

public interface INewsDAO extends IGenericDAO<NewsModel> {
    List<NewsModel> findByCategoryId(Long categoryId);

    Long save(NewsModel newsModel);

    List<NewsModel> findAll(Pageble pageble);

    int getTotalItem();
}
