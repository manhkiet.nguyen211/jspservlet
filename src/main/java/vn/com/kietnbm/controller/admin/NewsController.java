package vn.com.kietnbm.controller.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.com.kietnbm.cosntant.SystemConstant;
import vn.com.kietnbm.model.NewsModel;
import vn.com.kietnbm.paging.PageRequest;
import vn.com.kietnbm.paging.Pageble;
import vn.com.kietnbm.service.INewsService;
import vn.com.kietnbm.sort.Sorter;
import vn.com.kietnbm.utils.FormUtil;

@WebServlet(urlPatterns = { "/admin-new" })
public class NewsController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Inject
    private INewsService newsService;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        /*
         * NewsModel model = new NewsModel(); String pageStr =
         * request.getParameter("page"); String maxPageItemStr =
         * request.getParameter("maxPageItem"); if (pageStr != null) {
         * model.setPage(Integer.parseInt(pageStr)); } else { model.setPage(1);
         * } if (maxPageItemStr != null) {
         * model.setMaxPageItem(Integer.parseInt(maxPageItemStr)); }
         */
        NewsModel model = FormUtil.toModel(NewsModel.class, request);
        Pageble pageble = new PageRequest(model.getPage(),
                model.getMaxPageItem(),
                new Sorter(model.getSortName(), model.getSortBy()));
        /* Integer offset = (model.getPage() - 1) * model.getMaxPageItem(); */
        model.setListResult(newsService.findAll(pageble));
        model.setTotalItem(newsService.getTotalItem());
        if (model.getTotalItem() != null && model.getMaxPageItem() != null) {
            model.setTotalPage((int) Math.ceil(
                    (double) model.getTotalItem() / model.getMaxPageItem()));
        }
        request.setAttribute(SystemConstant.MODEL, model);
        RequestDispatcher rd = request
                .getRequestDispatcher("views/admin/news/list.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.doPost(request, response);
    }
}
